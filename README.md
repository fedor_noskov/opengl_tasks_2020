# Репозиторий для приема заданий по компьютерной графике // ФИВТ МФТИ 2019 // Семинарист Якимушкина А.О.
## План занятий и сдач
За изменениями следите в tg-канале.

Ссылки на семинары доступны в [Wiki проекта](https://bitbucket.org/arina5arina/opengl_tasks_2020/wiki/Home).

№ | Дата | Тема
-:|-------|------------------|
0 | 05 февраля | Презентация курса
1 | 12 февраля | [OpenGL pipeline](https://docs.google.com/presentation/d/18ZKk9kvYfxh9F_o5GDKR_1OE4UnPr_MUqmbpalIw5LU/edit?usp=sharing)
2 | 19 февраля | [Шейдеры](https://docs.google.com/presentation/d/1uJYBbgDtWnDAvi_6s3Pj6oR0mVJ0XIqXm6ch5_CqGaQ/edit?usp=sharing)
3 | 26 февраля | [Преобразование координат](https://docs.google.com/presentation/d/1eBSumG7krWX7rq-uxJxkizsKTfki4Q37lwLJiCayeCA/edit?usp=sharing)
- | 5 марта | Приём задания 1
4 | 12 марта | [Освещение](https://docs.google.com/presentation/d/1pqn_9y5ZwgFnJvZC8R1e491FpropmcjTigX9Q3SA9aA/edit?usp=sharing)
5 | 19 марта | [Текстурирование](https://docs.google.com/presentation/d/1SwN7d0OmrNxkXWL6SwJClo7G9xjl7IFDyKBG1U-drCk/edit?usp=sharing)
6 | 26 марта | [Тесты и смешивание](https://docs.google.com/presentation/d/1nQw-Giq03uVZzuJAu9F3sa1uBTzeEunEO9Ui6gYi_QQ/edit?usp=sharing)
7 | 2 апреля | [Тени](https://docs.google.com/presentation/d/1Tr9OVxMGlyI_Im_x7_aHA8-9hcedeJCGaytXkii6AU0/edit?usp=sharing)
8 | 9 апреля | [Deferred shading, SSAO, Gamma correction](https://docs.google.com/presentation/d/1tAH-Ol8kjvhmecO4rpiwy8h2I2pOwg9lgmSNijBxqok/edit?usp=sharing)
- | 16 апреля | Приём задания 2
9 | 23 апреля | [Instancing, Shaders](https://docs.google.com/presentation/d/1m594sHhUYBUVeLgh2WF5SLk-dw1JS_XQKChz0XuU3aY/edit?usp=sharing)
10 | 30 апреля | Conditional rendering, Geometry shader
11 | 7 мая | Compute shader, Tessellation shaders
- | 14 мая | Приём задания 3
- | 21 мая | Досдачи

## Краткое руководство по встраиванию

* Создайте форк этого репозитория.
* Для каждого задания выделена отдельная папка (*task1*, *task2*).
* В папке с заданием создайте свою папку `<номер группы><фамилия на латинице>` (например, *123Ivanov*), работайте только в этой папке.
* Создайте вложенную подпапку `<номер группы><фамилия на латинице>Data<номер задания>` (например, *123IvanovData1*). Используйте эту папку для размещения загружаемых в программе файлов (3D модели, изображения и т.д.).
* В папке `<номер группы><фамилия на латинице>` создайте файл CMakeLists.txt следующего содержимого

```
set(SRC_FILES
    Main.h
    Main.cpp
)

MAKE_OPENGL_TASK(123Ivanov 1 "${SRC_FILES}")
```

Здесь в переменной **SRC_FILES** укажите имена ваших файлов с исходным кодом.
    
В аргументах макроса **MAKE_OPENGL_TASK** укажите имя папки и номер задания (1, 2 или 3).

### Образец
В репозитории приведены примеры: задание 2 по OpenGL (скопированы из примеров к курсу).

### Зависимости
Не рекомендуется инклюдить файлы из примеров оформления задания: они общие, вдруг вам понадобится что-то поменять.
Скопируйте всё, что вам нужно.